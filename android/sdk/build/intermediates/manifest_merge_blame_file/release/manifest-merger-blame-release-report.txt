1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    xmlns:tools="http://schemas.android.com/tools"
4    package="org.jitsi.meet.sdk" >
5
6    <uses-sdk
7        android:minSdkVersion="23"
7-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml
8        android:targetSdkVersion="29" />
8-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml
9    <!-- XXX ACCESS_NETWORK_STATE is required by WebRTC. -->
10    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
10-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:6:5-79
10-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:6:22-76
11    <uses-permission android:name="android.permission.BLUETOOTH" />
11-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:7:5-68
11-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:7:22-65
12    <uses-permission android:name="android.permission.CAMERA" />
12-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:8:5-65
12-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:8:22-62
13    <uses-permission android:name="android.permission.INTERNET" />
13-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:9:5-67
13-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:9:22-64
14    <uses-permission android:name="android.permission.MANAGE_OWN_CALLS" />
14-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:10:5-75
14-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:10:22-72
15    <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
15-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:11:5-80
15-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:11:22-77
16    <uses-permission android:name="android.permission.RECORD_AUDIO" />
16-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:12:5-71
16-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:12:22-68
17    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />
17-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:13:5-78
17-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:13:22-75
18    <uses-permission android:name="android.permission.WAKE_LOCK" />
18-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:14:5-68
18-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:14:22-65
19    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
19-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:15:5-76
19-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:15:22-73
20    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
20-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:16:5-77
20-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:16:22-74
21
22    <uses-feature
22-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:18:5-20:35
23        android:glEsVersion="0x00020000"
23-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:19:9-41
24        android:required="true" />
24-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:20:9-32
25    <uses-feature
25-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:21:5-23:36
26        android:name="android.hardware.camera"
26-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:22:9-47
27        android:required="false" />
27-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:23:9-33
28    <uses-feature
28-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:24:5-26:36
29        android:name="android.hardware.camera.autofocus"
29-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:25:9-57
30        android:required="false" />
30-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:26:9-33
31
32    <application
32-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:28:5-59:19
33        android:allowBackup="true"
33-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:29:9-35
34        android:label="@string/app_name"
34-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:30:9-41
35        android:supportsRtl="true" >
35-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:31:9-35
36        <activity
36-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:32:9-38:57
37            android:name="org.jitsi.meet.sdk.JitsiMeetActivity"
37-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:33:13-46
38            android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|screenSize|smallestScreenSize"
38-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:34:13-115
39            android:launchMode="singleTask"
39-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:35:13-44
40            android:resizeableActivity="true"
40-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:36:13-46
41            android:supportsPictureInPicture="true"
41-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:37:13-52
42            android:windowSoftInputMode="adjustResize" />
42-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:38:13-55
43        <activity android:name="com.facebook.react.devsupport.DevSettingsActivity" />
43-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:39:9-86
43-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:39:19-83
44
45        <service
45-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:41:9-47:19
46            android:name="org.jitsi.meet.sdk.ConnectionService"
46-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:42:13-46
47            android:permission="android.permission.BIND_TELECOM_CONNECTION_SERVICE" >
47-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:43:13-84
48            <intent-filter>
48-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:44:13-46:29
49                <action android:name="android.telecom.ConnectionService" />
49-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:45:17-76
49-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:45:25-73
50            </intent-filter>
51        </service>
52        <service
52-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:49:9-51:63
53            android:name="org.jitsi.meet.sdk.JitsiMeetOngoingConferenceService"
53-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:50:13-80
54            android:foregroundServiceType="mediaProjection" />
54-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:51:13-60
55
56        <provider
56-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:53:9-58:20
57            android:name="com.reactnativecommunity.webview.RNCWebViewFileProvider"
57-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:54:13-83
58            android:authorities="${applicationId}.fileprovider"
58-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:55:13-64
59            android:enabled="false"
59-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:56:13-36
60            tools:replace="android:authorities" >
60-->/home/gibraan/Winjit/jitsi-meet-changes/android/sdk/src/main/AndroidManifest.xml:57:13-48
61        </provider>
62    </application>
63
64</manifest>
